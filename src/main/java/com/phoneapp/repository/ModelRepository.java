package com.phoneapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.phoneapp.model.Model;

@Repository
public interface ModelRepository extends JpaRepository<Model, Long>{
	
	public Model findByNazivModela(String nazivModela);

	public List<Model> findByMarkaId(Long markaId); 

}
