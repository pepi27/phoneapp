package com.phoneapp.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.phoneapp.model.Marka;
import com.phoneapp.web.dto.MarkaDTO;


@Component
public class MarkaToMarkaDTO implements Converter<Marka, MarkaDTO>{

	@Override
	public MarkaDTO convert(Marka marka) {
		MarkaDTO markaDto = new MarkaDTO(); 
		markaDto.setId(marka.getId());
		markaDto.setNazivMarke(marka.getNazivMarke());
		return markaDto;
	}
	
	public List<MarkaDTO> convert(List<Marka> marke) {
		List<MarkaDTO> markeDto = new ArrayList<>(); 
		for(Marka m : marke) {
			markeDto.add(convert(m)); 
		}
		return markeDto;
	}
}