package com.phoneapp.service.implementation;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.phoneapp.model.Marka;
import com.phoneapp.repository.MarkaRepository;
import com.phoneapp.service.MarkaService;

@Service
@Transactional
public class MarkaServiceImpl implements MarkaService {

	@Autowired
	private MarkaRepository markaRepository; 
	
	@Override
	public Marka findOne(Long id) {
		return markaRepository.findOne(id);
	}

	@Override
	public void remove(Long id) {
		markaRepository.delete(id);
	}

	@Override
	public List<Marka> findAll() {
		return markaRepository.findAllOrderByNazivMarke();
	}

	@Override
	public void save(Marka marka) {
		markaRepository.save(marka);
	}

	@Override
	public Marka findByNazivMarke(String marka) {
		return markaRepository.findByNazivMarke(marka);
	}
}