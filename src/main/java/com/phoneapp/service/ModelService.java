package com.phoneapp.service;

import java.util.List;

import com.phoneapp.model.Model;

public interface ModelService {

	Model findOne(Long id);
	void remove(Long id);
	List<Model> findAll();
	void save(Model model);
	Model findByNazivModela(String model);
	List<Model> findByMarkaId(Long markaId); 
}
