package com.phoneapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Length.List;

@Entity
@Table
public class Telefon {

	@Id
	@GeneratedValue
	@Column
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Model model; 
	@Enumerated(EnumType.STRING)
	private Stanje stanje;
	@Column
	private Double cena; 
	@Column
	@Length(max = 255, message = "Polje moze sadrzati najvise 255 karaktera")
	private String opis;
	@Column 
	private Date datumUnosa;
	@Column
	private String ipAddress;
	@Column
	private String kontakt;
	
	
	public Telefon() {
	}

	public Telefon(Long id, Model model, Stanje stanje, Double cena, String opis) {
		super();
		this.id = id;
		this.model = model;
		this.stanje = stanje;
		this.cena = cena;
		this.opis = opis;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Stanje getStanje() {
		return stanje;
	}

	public void setStanje(Stanje stanje) {
		this.stanje = stanje;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public Date getDatumUnosa() {
		return datumUnosa;
	}

	public void setDatumUnosa(Date datumUnosa) {
		this.datumUnosa = datumUnosa;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getKontakt() {
		return kontakt;
	}

	public void setKontakt(String kontakt) {
		this.kontakt = kontakt;
	}
}