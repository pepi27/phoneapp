package com.phoneapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Marka {

	@Id
	@GeneratedValue
	@Column
	private Long id; 
	@Column
	private String nazivMarke;

	public Marka() {
	}

	public Marka(Long id, String nazivMarke) {
		super();
		this.id = id;
		this.nazivMarke = nazivMarke;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazivMarke() {
		return nazivMarke;
	}

	public void setNazivMarke(String nazivMarke) {
		this.nazivMarke = nazivMarke;
	}
}